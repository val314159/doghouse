package io.ccl.doghouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoghouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoghouseApplication.class, args);
	}

}
