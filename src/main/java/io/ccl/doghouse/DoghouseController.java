package io.ccl.doghouse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DoghouseController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	model.addAttribute("name", name);
	return "greeting";
    }

    @GetMapping("/main")
    public String main(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	model.addAttribute("name", name);
	return "main";
    }

    @GetMapping("/about")
    public String about(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	//model.addAttribute("name", name);
	
	model.addAttribute("Name", "Bosco and Winston's Doghouse");
	model.addAttribute("Address", "21 Jump Street");
	model.addAttribute("City", "Anytown");
	model.addAttribute("State", "CA");
	model.addAttribute("Zip", "90210");

	model.addAttribute("Phone", "505-867-5309");

	return "about";
    }

}
