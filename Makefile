all::
	tailwind -o src/site.css
	./gradlew build
clean::
	find . -name \*~ | xargs rm -fr
